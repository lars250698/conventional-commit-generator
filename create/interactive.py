from create import from_args

def generate() -> str:
    type = input('Type (required): ')
    scope = input('Scope: ')
    breaking = input('Breaking (y/N): ') == 'y'
    message = input('Message (required): ')
    body = input('Body: ')
    footer = input('Footer: ')

    return from_args.generate(type, scope, breaking, message, body, footer)