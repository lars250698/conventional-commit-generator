
from typing import Dict, List

valid_types: List[str] = ['feat', 'fix', 'docs', 'style', 'refactor', 'perf', 'test', 'chore', 'revert', 'build', 'ci', 'wip']


def generate(type: str, scope: str, breaking: bool, message: str, body: str, footer: str) -> str:
    msg = 'git commit -m '
    msg += f'"{generate_header(type, scope, breaking, message)}"'
    if body != '':
        msg += f' -m "{body}"'
    if footer != '':
        msg += f' -m "{footer}"'
    return msg

def generate_header(type: str, scope: str, breaking: bool, message: str) -> str:
    check_type(type)
    check_message(message)

    msg = ''
    msg += type

    if scope != '':
        msg += f'({scope})'
    
    if breaking:
        msg += '!'

    msg += f': {message}'
    return msg


def check_type(type: str) -> bool:
    if type == '':
        print('No type provided!')
        exit(1)
    if type not in valid_types:
        print(f'Invalid type: {type}')
        exit(1)

def check_message(message: str) -> bool:
    if message == '':
        print('No message provided!')
        exit(1)
