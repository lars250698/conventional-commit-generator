#!/usr/bin/python3
import argparse
import sys
from create import interactive, from_args

parser = argparse.ArgumentParser(description='Generate a conventional commit message.')
parser.add_argument('-i', '--interactive', help='Interactive mode', default=False, action='store_true')
parser.add_argument('-t', '--type', help='Commit type', default='')
parser.add_argument('-s', '--scope', help='Scope', default='')
parser.add_argument('-B', '--breaking', help='Breaking change', default=False, action='store_true')
parser.add_argument('-m', '--message', help='Commit message', default='')
parser.add_argument('-b', '--body', help='Commit body', default='')
parser.add_argument('-f', '--footer', help='Footer', default='')
args = parser.parse_args()

if not len(sys.argv) > 1:
    args.interactive = True

if args.interactive:
    msg = interactive.generate()
    msg = f'\n{msg}'
else:
    msg = from_args.generate(args.type, args.scope, args.breaking, args.message, args.body, args.footer)

print(msg)
exit(0)
